import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private router:Router) { }

  ngOnInit(): void {
  }
  isLoading!:boolean;
login(){
  this.isLoading=true;
  setTimeout(()=>{
this.router.navigate(['/']);
  },2000);
}

}
