import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BlogPageComponent } from './blog/blog-page/blog-page.component';
import { PostComponent } from './blog/post/post.component';
import { LoginComponent } from './login/login.component';

 

const routes: Routes = [
  {path:'login',component:LoginComponent},
  {path:'blog',component:BlogPageComponent},
  {path:'blog/best-blog',component:BlogPageComponent},
  {path:'admin/blog/best-blog',component:PostComponent}


];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
